<?php

namespace app\admin\controller;
use think\Session;
use think\Db;
use think\Controller;
use think\Request;

class Login extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
//    后台登录跳转页面
    public function login()
    {
        return $this->fetch('login');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
//    后台接收表单,执行登录
    public function doLogin()
    {
//        return 'dologin';
        $param = input('post.');
        $username = $param['username'];
        $userpwd = $param['userpwd'];

        $admin = Db::table('phper')->where('user_name','=',$username)->find();
//        var_dump($admin);die;
        if($admin){
            if($admin['user_pwd']== md5($userpwd)&& ($admin['user_name']=$username)){
//                将登录id和名称存入session
                Session::set('id',$admin['id']);
                Session::set('username',$admin['user_name']);

                return $this->success('登录成功','admin/Index/index');
//                return $this->fetch();
            }else{

                return $this->error('密码不正确','admin/Login/login');
            }
        }else{
                return $this->error('用户不存在','admin/Login/login');

        }


    }
//后台退出
    public function logout()
    {
        Session::set(null); //退出清空session
        return $this->success('退出成功',url('admin/Login/login'));
    }


}
