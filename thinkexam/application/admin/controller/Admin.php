<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Request;

class Admin extends Base
{
//    个人信息
public function person()
{
    return $this->fetch('admin-person');
}


//   管理员列表
    public function adminlist()
    {
        return $this->fetch('admin-list');
    }

//   角色管理
    public function adminrole()
    {
        return $this->fetch('admin-role');

    }

//    权限分类
    public function admincate()
    {
        return $this->fetch('admin-cate');

    }

//    权限管理
    public function adminrule()
    {
        return $this->fetch('admin-rule');

    }

//    管理员添加
    public function adminadd()
    {
        return $this->fetch('admin-add');
    }

//   角色添加
    public function roleadd()
    {
        return $this->fetch('role-add');
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
