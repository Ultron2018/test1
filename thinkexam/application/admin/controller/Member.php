<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Request;

class Member extends Base
{
//    显示会员列表
    public function index()
    {
        return $this->fetch('member-list');
    }
//删除会员列表
    public function delete()
    {
        return $this->fetch('member-del');
    }

//    会员添加
    public function memberadd()
    {
        return $this->fetch('member-add');
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */

}
