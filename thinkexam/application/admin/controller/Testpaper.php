<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Request;

class Testpaper extends Base
{
//   显示单选题库列表
    public function singleselect()
    {
        return $this->fetch('singleselection-list');
    }

//   显示多选题库列表
    public function  multiselect()
    {
        return $this->fetch('multiselect');
    }

//    添加试题,单选或多选
    public function addquestion()
    {
        return $this->fetch('addquestion-list');
    }




    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
