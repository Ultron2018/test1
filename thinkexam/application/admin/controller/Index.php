<?php
namespace app\admin\controller;

use app\admin\common\Base;
use think\Cache;
use think\cache\driver\Redis;
use think\Session;

class Index extends Base
{
//    后台首页控制器
    public function index()
    {
//        在后台首页显示谁登录
        $adminname = Session::get('username');
        $this->assign([
            'adm'=>$adminname,
        ]);
//        return '后台首页模块';
       return $this->fetch('index');

    }

    public function welcome()
    {
//        return '欢迎页面';
        return $this->fetch('welcome');
    }


}

