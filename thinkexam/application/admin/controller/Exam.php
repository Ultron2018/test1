<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Request;

class Exam extends Base
{

//    正在考试的试卷列表
    public function testing()
    {
        return $this->fetch('testing-list');
    }

//已经考完试的试卷列表
    public function tested()
    {
        return $this->fetch('tested-list');

    }

//    添加试卷的考试列表
    public function addtest()
    {
        return $this->fetch('addtest-list');

    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
