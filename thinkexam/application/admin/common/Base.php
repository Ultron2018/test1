<?php
/**
 * Created by PhpStorm.
 * User: Search
 * Date: 2018/4/15
 * Time: 18:26
 */

namespace app\admin\common;
use think\Controller;
use think\Session;

class Base extends Controller
{
    public function _initialize()
    {
        $uid = Session::get('id');
//        var_dump($uid);
        if($uid == null){
            $this->error('请先登录后操作','admin/login/login');
        }
    }
}