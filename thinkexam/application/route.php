<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;


Route::rule('admin','admin/Index/index');
Route::rule('login','admin/login/login');
Route::rule('doLogin','admin/login/doLogin');
Route::rule('logout','admin/login/logout');
Route::rule('welcome','admin/index/welcome');
Route::rule('person','admin/admin/person');

//会员列表
Route::rule('member','admin/member/index');
Route::rule('memberdel','admin/member/delete');
Route::rule('memberadd','admin/member/memberadd');

//试卷管理路由规则
Route::rule('singleselect','admin/testpaper/singleselect');
Route::rule('multiselect','admin/testpaper/multiselect');
Route::rule('addquestion','admin/testpaper/addquestion');

//考试管理路由规则
Route::rule('testing','admin/exam/testing');
Route::rule('tested','admin/exam/tested');
Route::rule('addtest','admin/exam/addtest');

//管理员管理路由规则
Route::rule('adminlist','admin/admin/adminlist');
Route::rule('adminrole','admin/admin/adminrole');
Route::rule('admincate','admin/admin/admincate');
Route::rule('adminrule','admin/admin/adminrule');
Route::rule('adminadd','admin/admin/adminadd');
Route::rule('roleadd','admin/admin/roleadd');

